
install_pkgs() {
    apt-get update
    apt-get install -y curl git tar
}

install_helm() {
    curl -fLSs https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz -o helm.tar.gz
    tar -xvf helm.tar.gz
    mv linux-amd64/helm /usr/local/bin/helm
    helm plugin install https://github.com/chartmuseum/helm-push.git
}
