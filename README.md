# External Helm charts

:warning: Repository archived now that
[kubitus-project/kubitus-installer!435](https://gitlab.com/kubitus-project/kubitus-installer/-/merge_requests/435)
*Use patches_strategic_merge for sealed-secrets*
is merged.

## Sealed Secrets

Usage:
```
helm repo add sealed-secrets https://gitlab.com/api/v4/projects/30572334/packages/helm/stable
--
helm repo add sealed-secrets https://gitlab.com/api/v4/projects/30572334/packages/helm/<branch-name>

helm install sealed-secrets/sealed-secrets
```
Available values are listed here.
